package com.helmes.app.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.helmes.app.domain.Sector;
import com.helmes.app.repository.SectorRepository;

@Service
public class SectorService {
	
	public static final String DELIMETER = "\u00A0";
	public static final String DELIMETER_PATTERN = DELIMETER + "+";
	public static final Integer DELIMETERS_PER_LEVEL = 4;
	
	private SectorRepository sectorRepository;
	
	@Autowired
	public SectorService(SectorRepository sectorRepository){
		this.sectorRepository = sectorRepository;
	}
	
	/**
	   * Returns all Sectors as children-parent view
	   * @return list of Sectors
	   */
	public List<Sector> getAllSectorsAsTreeView(){
		List<Sector> result = new ArrayList<Sector>();
		List<Sector> sectors = this.sectorRepository.findAll();
		for(Sector sector : sectors){
			sector.setTitle(concatDelimeters(sector.getLevel()) + sector.getTitle());
			result.add(sector);
		}
		return result;
	}
	
	private String concatDelimeters(Integer concatTimes){
		StringBuffer sb = new StringBuffer();
		for(int i=0; i<concatTimes*DELIMETERS_PER_LEVEL; i++){
			sb.append(DELIMETER);
		}
		return sb.toString();
	}

}
