package com.helmes.app.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.helmes.app.domain.Employee;
import com.helmes.app.repository.EmployeeRepository;
import com.helmes.app.service.SectorService;

@Controller
@RequestMapping("/employee")
@SessionAttributes("employee")
public class EmployeeController {
	
	private static Logger LOG = LoggerFactory.getLogger(EmployeeController.class);
	
	private EmployeeRepository employeeRepository;
	private SectorService sectorService;
	
	@Autowired
	public EmployeeController(EmployeeRepository employeeRepository, SectorService sectorService){
		this.employeeRepository = employeeRepository;
		this.sectorService = sectorService;
	}
	
	@GetMapping("")
	public String view(HttpServletRequest request, Model model){
		model.addAttribute("sectorsList", this.sectorService.getAllSectorsAsTreeView());
		return "employee";
	}
	
	@PostMapping("/save")
	public String save(HttpServletRequest request, @ModelAttribute("employee") @Valid Employee employee, 
			BindingResult bindingResult, Model model, RedirectAttributes attr){
		if(bindingResult.hasErrors()){
			LOG.error("Error occured validating user input data: {}", bindingResult.toString());
			attr.addFlashAttribute("org.springframework.validation.BindingResult.employee", bindingResult);
			attr.addFlashAttribute("employee", employee);
			attr.addFlashAttribute("messageCode", "common.save.error");
			return "redirect:../employee";
		}
		Employee newEmployee = this.employeeRepository.save(employee);
		model.addAttribute("employee", newEmployee);
		attr.addFlashAttribute("messageCode", "common.save.success");
		return "redirect:../employee";
	}
	
	@ModelAttribute("employee")
    public Employee getEmployeeForm() {
        return new Employee();
    }
}
