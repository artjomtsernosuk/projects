package com.helmes.app.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.helmes.app.domain.Employee;

@Controller
@RequestMapping("/")
public class CommonController implements ErrorController{
	
	private static Logger LOG = LoggerFactory.getLogger(CommonController.class);
	
	private static final String ERROR_PATH = "/error";
	private final ErrorAttributes errorAttributes;
	
	@Autowired
	public CommonController(ErrorAttributes errorAttributes) {
		this.errorAttributes = errorAttributes;
	}
	
	@RequestMapping(method=RequestMethod.GET)
	public String index(HttpServletRequest request, Model model){
		model.addAttribute("employee", new Employee());
		return "employee";
	}
	
	@RequestMapping(value=ERROR_PATH, method={RequestMethod.POST, RequestMethod.GET})
	public String error(HttpServletRequest request){
		Map<String, Object> errorBody = this.errorAttributes.getErrorAttributes
				(new ServletRequestAttributes(request), getTraceParameter(request));
		LOG.error(errorBody.get("message").toString());
		return "error";
	}
	
	/**
	   * Determines, if stack trace elements should be included in the error attributes
	   * @param request
	   * @return true if request contains stack trace elements, false otherwise
	   */
	private boolean getTraceParameter(HttpServletRequest request) {
        String parameter = request.getParameter("trace");
        if (parameter == null) {
            return false;
        }
        return !"false".equals(parameter.toLowerCase());
    }

	@Override
	public String getErrorPath() {
		return ERROR_PATH;
	}

}
