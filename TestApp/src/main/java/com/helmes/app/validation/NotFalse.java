package com.helmes.app.validation;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({TYPE, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = NotFalseValidator.class)
@Documented
public @interface NotFalse{
	
    String message() default "NotFalse";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
    
    String field();
    
    String errorMessage();

    /**
     * Defines several <code>@NotFalse</code> annotations on the same element
     *
     * @see NotFalse
     */
    @Target({TYPE, ANNOTATION_TYPE})
    @Retention(RUNTIME)
    @Documented
    @interface List{
    	NotFalse[] value();
    }
}