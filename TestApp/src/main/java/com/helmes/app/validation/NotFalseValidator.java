package com.helmes.app.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.beanutils.BeanUtils;

public class NotFalseValidator implements ConstraintValidator<NotFalse, Object> {
	
	private String fieldName;
	private String errorMessage;
    
    @Override
    public void initialize(NotFalse flag) {
    	this.fieldName = flag.field();
    	this.errorMessage = flag.errorMessage();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context){
        try{
        	boolean isValid = true;
        	if(org.springframework.beans.BeanUtils.findPropertyType(fieldName, value.getClass()).equals(Boolean.class)){
        		isValid = Boolean.valueOf(BeanUtils.getProperty(value, fieldName));
        	}else{
        		isValid = false;
        	}
            if(!isValid){
            	context.disableDefaultConstraintViolation();
            	context.buildConstraintViolationWithTemplate(errorMessage)
                        .addPropertyNode(fieldName)
                        .addConstraintViolation();
            }
            return isValid;
        } catch (final Exception ignore){
            // ignore
        }
        return true;
    }
}
