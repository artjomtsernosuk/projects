package com.helmes.app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.helmes.app.domain.Sector;
import com.helmes.app.repository.SectorRepository;
import com.helmes.app.service.SectorService;

@SpringBootApplication
public class App {
	
	private static Logger LOG = LoggerFactory.getLogger(App.class);
	
	@Autowired
	private SectorRepository sectorRepository;

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(App.class);
		app.setBannerMode(Banner.Mode.OFF);
		app.run(args);
	}
	
	@PostConstruct
    public void parseSectorsFromHTML() {
		List<Sector> sectors = sectorRepository.findAll();
		if(sectors.isEmpty()){
			LOG.info("No Sector entities found in DB, parsing sectors...");
			BufferedReader br = null;
	        String line = null;
	        final StringBuilder sb = new StringBuilder(256);
	        try {
	            br = new BufferedReader(new InputStreamReader(Thread.currentThread().getContextClassLoader()
	                    .getResourceAsStream("index.html")));
	            while ((line = br.readLine()) != null) {
	                sb.append(line);
	            }
	        } catch (final IOException e) {
	        	LOG.error("Can't read file: {}", e.getMessage(), e);
	        } finally {
	            if (br != null) {
	                try {
	                    br.close();
	                } catch (IOException e) {
	                }
	            }
	        }
	        Document document = Jsoup.parse(sb.toString());
	        Elements options = document.select("select > option");
	        int prevOccurrences = 0;
	        Sector parent = null;
	        Sector prevParent = null;
	        for(Element element : options){
	        	int curOccurrences = StringUtils.countOccurrencesOf(element.html(), "&nbsp;");
	        	if(curOccurrences == 0){
	        		parent = this.sectorRepository.save(
	        				new Sector(
	        						element.text().replaceAll(SectorService.DELIMETER_PATTERN, ""), 
	        						curOccurrences)
	        		);
	        	}else{
	        		if(prevOccurrences < curOccurrences){
	        			prevParent = parent;
	            	}else if(prevOccurrences > curOccurrences){
	            		prevParent = getParentByLevel(parent, 
	            				(prevOccurrences - curOccurrences)/SectorService.DELIMETERS_PER_LEVEL);
	            	}
	        		parent = this.sectorRepository.save(
	        				new Sector(
	        						element.text().replaceAll(SectorService.DELIMETER_PATTERN, ""), 
	        						prevParent.getLevel()+1, prevParent)
	        		);
	        	}
	        	prevOccurrences = curOccurrences;
	        }
		}
    }
	
	@Bean
    public LocaleResolver localeResolver() {
		SessionLocaleResolver resolver = new SessionLocaleResolver();
		resolver.setDefaultLocale(Locale.UK);
		return resolver;
    }
	
	@Bean
	public ResourceBundleMessageSource messageSource() {
		ResourceBundleMessageSource source = new ResourceBundleMessageSource();
		source.setBasenames("messages");
		return source;
	}
	
	/**
	   * Returns parent Sector of the certain level
	   * @param curSector - current Sector
	   * @param levelsDown - how many levels down the tree should search
	   * @return parent Sector of the certain level
	   */
	private Sector getParentByLevel(Sector curSector, Integer levelsDown){
		Sector parent = curSector;
		for(int i = 0; i < levelsDown + 1; i++){
			parent = parent.getParent();
		}
		return parent;
	}
}
