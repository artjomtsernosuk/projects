package com.helmes.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.helmes.app.domain.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long>{

}
