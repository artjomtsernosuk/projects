package com.helmes.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.helmes.app.domain.Sector;

public interface SectorRepository extends JpaRepository<Sector, Long> {

}
