package com.helmes.app.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import com.helmes.app.validation.NotFalse;

@Entity
@Table
@NotFalse.List({
    @NotFalse(field = "agreedToTerms", errorMessage = "Must accept terms")
})
public class Employee extends BaseEntity{
	
	private static final long serialVersionUID = 1L;
	
	@NotBlank
	private String name;
	@NotNull
	private Boolean agreedToTerms;
	@NotEmpty
	@ManyToMany(fetch=FetchType.EAGER)
	private Collection<Sector> sectors;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Boolean getAgreedToTerms() {
		return agreedToTerms;
	}

	public void setAgreedToTerms(Boolean agreedToTerms) {
		this.agreedToTerms = agreedToTerms;
	}

	public Collection<Sector> getSectors() {
		return sectors;
	}

	public void setSectors(Collection<Sector> sectors) {
		this.sectors = sectors;
	}
	
	@Transient
	public Collection<Long> getSectorIds(){
		List<Long> ids = new ArrayList<Long>();
		if(this.sectors != null){
			for(Sector sector : this.sectors){
				ids.add(sector.getId());
			}
		}
		return ids;
	}

	@Override
	public String toString() {
		return "Employee [name=" + name + ", agreedToTerms=" + agreedToTerms
				+ ", sectors=" + sectors + "]";
	}

}
