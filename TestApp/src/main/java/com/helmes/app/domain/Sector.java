package com.helmes.app.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table
public class Sector extends BaseEntity{

	private static final long serialVersionUID = 1L;
	
	private String title;
	private Integer level;
	@ManyToOne
	@JsonBackReference
    private Sector parent;
    @OneToMany(mappedBy="parent")
    @JsonManagedReference
    private Set<Sector> children = new HashSet<Sector>();
    
    public Sector(){}
    
    public Sector(String title, Integer level){
    	this.title = title;
    	this.level = level;
    }
    
    public Sector(String title, Integer level, Sector parent){
    	this.title = title;
    	this.level = level;
    	this.parent = parent;
    }
    
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Sector getParent() {
		return parent;
	}

	public void setParent(Sector parent) {
		this.parent = parent;
	}

	public Set<Sector> getChildren() {
		return children;
	}

	public void setChildren(Set<Sector> children) {
		this.children = children;
	}
	
	public void addChild(Sector child) {
		this.children.add(child);
	}
	
}
