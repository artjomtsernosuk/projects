package com.helmes.app.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@MappedSuperclass
public abstract class BaseEntity implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id  
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)  
    protected Long id;
	
	@Column(name = "created_at", nullable = false, updatable = false)  
    @Temporal(TemporalType.TIMESTAMP)  
    private Date createdAt;
	
	@Column(name = "updated_at", nullable = false)  
    @Temporal(TemporalType.TIMESTAMP)  
    private Date updatedAt;
	
	public Long getId(){
        return id;
    }
	
	public void setId(Long id){
        this.id = id;
	}
	
	public Date getCreatedAt(){
        return createdAt;
	}

	public Date getUpdatedAt(){
        return updatedAt;
	}
	
    @PrePersist
    public void setCreationDate(){
        this.createdAt = this.updatedAt = new Date();
    }
  
    @PreUpdate
    public void setChangeDate(){
        this.updatedAt = new Date();
    }
}
