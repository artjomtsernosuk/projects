DROP SEQUENCE IF EXISTS "employee_id_seq";

DROP SEQUENCE IF EXISTS "sector_id_seq";

CREATE TABLE employee (
    id bigint NOT NULL DEFAULT nextval(('"employee_id_seq"'::text)::regclass),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    agreed_to_terms boolean NOT NULL,
    name character varying(255) NOT NULL
);

CREATE TABLE employee_sectors (
    employee_id bigint NOT NULL,
    sectors_id bigint NOT NULL
);

CREATE TABLE sector (
    id bigint NOT NULL DEFAULT nextval(('"sector_id_seq"'::text)::regclass),
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    level integer,
    title character varying(255),
    parent_id bigint
);

CREATE SEQUENCE "employee_id_seq" INCREMENT 1 START 1;

CREATE SEQUENCE "sector_id_seq" INCREMENT 1 START 1;
    
ALTER TABLE ONLY employee
    ADD CONSTRAINT PK_employee PRIMARY KEY (id);
    
ALTER TABLE ONLY sector
    ADD CONSTRAINT PK_sector PRIMARY KEY (id);
    
ALTER TABLE ONLY employee_sectors
    ADD CONSTRAINT FK_employee_sectors_sector FOREIGN KEY (sectors_id) REFERENCES sector(id);

ALTER TABLE ONLY sector
    ADD CONSTRAINT FK_sector FOREIGN KEY (parent_id) REFERENCES sector(id);

ALTER TABLE ONLY employee_sectors
    ADD CONSTRAINT FK_employee_sectors_employee FOREIGN KEY (employee_id) REFERENCES employee(id);