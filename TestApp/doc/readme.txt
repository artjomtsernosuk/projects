1. Specification
Application is using Spring Boot version 1.5.1, based on Spring framework 5. Gradle as a project building tool.
PostgreSQL 9.4 as a datasource and Flyway 4 is used for database migrations.

2. Configuring application
Application configuration file is under <root_folder>/src/main/resources/application.properties.

3. Starting application

3.1 Before first start
Before starting application for the very first time, you should change some properties in configuration file.
First of all property <spring.datasource.url> should be changed, providing your own configuration properties jdbc:postgresql://<host>:<port>/<db_name>.
Make sure that database with <db_name> really exists in the desired <host>.
Then provide configuration file with your own database authentication credentials <spring.datasource.username> and <spring.datasource.password>.
During first start application will automatically create database schema (tables, sequences, constraints) and insert some 
predefined values (currently only Sectors) into configured <db_name> database.

3.2 Start
Start application using App.java class. After application is successfully started you can use it navigating in your browser to http://<host>:8080

4. Resources
Full database dump used during application development and testing is situated under <root_folder>/doc/db_dump.sql.
Sectors are imported into database before first application start and those are parsed from file <root_folder>/src/main/resources/index.html.
Flyway database migration scripts are situated under <root_folder>/src/main/resources/db/migration directory and script naming 
pattern is <VyyyyMMdd__script_name>.sql