--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.0
-- Dumped by pg_dump version 9.6.0

-- Started on 2017-02-10 08:32:41

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE IF EXISTS app_db;
--
-- TOC entry 2156 (class 1262 OID 58893)
-- Name: app_db; Type: DATABASE; Schema: -; Owner: -
--

CREATE DATABASE app_db WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Estonian_Estonia.1257' LC_CTYPE = 'Estonian_Estonia.1257';


\connect app_db

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12387)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2158 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 186 (class 1259 OID 58904)
-- Name: employee; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE employee (
    id bigint DEFAULT nextval(('"employee_id_seq"'::text)::regclass) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    agreed_to_terms boolean NOT NULL,
    name character varying(255) NOT NULL
);


--
-- TOC entry 189 (class 1259 OID 58915)
-- Name: employee_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE employee_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 187 (class 1259 OID 58908)
-- Name: employee_sectors; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE employee_sectors (
    employee_id bigint NOT NULL,
    sectors_id bigint NOT NULL
);


--
-- TOC entry 185 (class 1259 OID 58894)
-- Name: schema_version; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE schema_version (
    installed_rank integer NOT NULL,
    version character varying(50),
    description character varying(200) NOT NULL,
    type character varying(20) NOT NULL,
    script character varying(1000) NOT NULL,
    checksum integer,
    installed_by character varying(100) NOT NULL,
    installed_on timestamp without time zone DEFAULT now() NOT NULL,
    execution_time integer NOT NULL,
    success boolean NOT NULL
);


--
-- TOC entry 188 (class 1259 OID 58911)
-- Name: sector; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE sector (
    id bigint DEFAULT nextval(('"sector_id_seq"'::text)::regclass) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    level integer,
    title character varying(255),
    parent_id bigint
);


--
-- TOC entry 190 (class 1259 OID 58917)
-- Name: sector_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE sector_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2147 (class 0 OID 58904)
-- Dependencies: 186
-- Data for Name: employee; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO employee (id, created_at, updated_at, agreed_to_terms, name) VALUES (1, '2017-02-09 15:49:33.629', '2017-02-09 15:49:33.629', true, 'rtret');
INSERT INTO employee (id, created_at, updated_at, agreed_to_terms, name) VALUES (2, '2017-02-09 15:52:38.438', '2017-02-09 15:52:38.438', true, 'test1');
INSERT INTO employee (id, created_at, updated_at, agreed_to_terms, name) VALUES (3, '2017-02-09 16:00:09.794', '2017-02-09 16:00:09.794', true, 'yo');
INSERT INTO employee (id, created_at, updated_at, agreed_to_terms, name) VALUES (4, '2017-02-09 16:04:45.567', '2017-02-09 16:05:14.921', true, 'test_update');
INSERT INTO employee (id, created_at, updated_at, agreed_to_terms, name) VALUES (5, '2017-02-09 16:07:35.24', '2017-02-09 16:07:35.24', true, 'dwerew');
INSERT INTO employee (id, created_at, updated_at, agreed_to_terms, name) VALUES (6, '2017-02-09 16:11:19.369', '2017-02-09 16:11:19.369', true, 'fgerg');


--
-- TOC entry 2159 (class 0 OID 0)
-- Dependencies: 189
-- Name: employee_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('employee_id_seq', 6, true);


--
-- TOC entry 2148 (class 0 OID 58908)
-- Dependencies: 187
-- Data for Name: employee_sectors; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO employee_sectors (employee_id, sectors_id) VALUES (1, 1);
INSERT INTO employee_sectors (employee_id, sectors_id) VALUES (1, 3);
INSERT INTO employee_sectors (employee_id, sectors_id) VALUES (1, 5);
INSERT INTO employee_sectors (employee_id, sectors_id) VALUES (2, 1);
INSERT INTO employee_sectors (employee_id, sectors_id) VALUES (2, 3);
INSERT INTO employee_sectors (employee_id, sectors_id) VALUES (2, 5);
INSERT INTO employee_sectors (employee_id, sectors_id) VALUES (3, 2);
INSERT INTO employee_sectors (employee_id, sectors_id) VALUES (3, 3);
INSERT INTO employee_sectors (employee_id, sectors_id) VALUES (3, 4);
INSERT INTO employee_sectors (employee_id, sectors_id) VALUES (4, 1);
INSERT INTO employee_sectors (employee_id, sectors_id) VALUES (4, 2);
INSERT INTO employee_sectors (employee_id, sectors_id) VALUES (4, 4);
INSERT INTO employee_sectors (employee_id, sectors_id) VALUES (5, 2);
INSERT INTO employee_sectors (employee_id, sectors_id) VALUES (5, 5);
INSERT INTO employee_sectors (employee_id, sectors_id) VALUES (5, 8);
INSERT INTO employee_sectors (employee_id, sectors_id) VALUES (6, 1);
INSERT INTO employee_sectors (employee_id, sectors_id) VALUES (6, 4);


--
-- TOC entry 2146 (class 0 OID 58894)
-- Dependencies: 185
-- Data for Name: schema_version; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO schema_version (installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) VALUES (1, '20170208', 'DB create', 'SQL', 'V20170208__DB_create.sql', 1889238826, 'postgres', '2017-02-09 15:33:25.821785', 65, true);


--
-- TOC entry 2149 (class 0 OID 58911)
-- Dependencies: 188
-- Data for Name: sector; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (1, '2017-02-09 15:33:33.545', '2017-02-09 15:33:33.545', 0, 'Manufacturing', NULL);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (2, '2017-02-09 15:33:33.609', '2017-02-09 15:33:33.609', 1, 'Construction materials', 1);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (3, '2017-02-09 15:33:33.614', '2017-02-09 15:33:33.614', 1, 'Electronics and Optics', 1);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (4, '2017-02-09 15:33:33.62', '2017-02-09 15:33:33.62', 1, 'Food and Beverage', 1);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (5, '2017-02-09 15:33:33.629', '2017-02-09 15:33:33.629', 2, 'Bakery & confectionery products', 4);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (6, '2017-02-09 15:33:33.632', '2017-02-09 15:33:33.632', 2, 'Beverages', 4);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (7, '2017-02-09 15:33:33.636', '2017-02-09 15:33:33.636', 2, 'Fish & fish products', 4);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (8, '2017-02-09 15:33:33.638', '2017-02-09 15:33:33.638', 2, 'Meat & meat products', 4);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (9, '2017-02-09 15:33:33.64', '2017-02-09 15:33:33.64', 2, 'Milk & dairy products', 4);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (10, '2017-02-09 15:33:33.647', '2017-02-09 15:33:33.647', 2, 'Other', 4);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (11, '2017-02-09 15:33:33.651', '2017-02-09 15:33:33.651', 2, 'Sweets & snack food', 4);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (12, '2017-02-09 15:33:33.654', '2017-02-09 15:33:33.654', 1, 'Furniture', 1);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (13, '2017-02-09 15:33:33.659', '2017-02-09 15:33:33.659', 2, 'Bathroom/sauna', 12);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (14, '2017-02-09 15:33:33.663', '2017-02-09 15:33:33.663', 2, 'Bedroom', 12);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (15, '2017-02-09 15:33:33.667', '2017-02-09 15:33:33.667', 2, 'Children''s room', 12);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (16, '2017-02-09 15:33:33.67', '2017-02-09 15:33:33.67', 2, 'Kitchen', 12);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (17, '2017-02-09 15:33:33.681', '2017-02-09 15:33:33.681', 2, 'Living room', 12);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (18, '2017-02-09 15:33:33.684', '2017-02-09 15:33:33.684', 2, 'Office', 12);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (19, '2017-02-09 15:33:33.688', '2017-02-09 15:33:33.688', 2, 'Other (Furniture)', 12);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (20, '2017-02-09 15:33:33.692', '2017-02-09 15:33:33.692', 2, 'Outdoor', 12);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (21, '2017-02-09 15:33:33.694', '2017-02-09 15:33:33.694', 2, 'Project furniture', 12);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (22, '2017-02-09 15:33:33.697', '2017-02-09 15:33:33.697', 1, 'Machinery', 1);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (23, '2017-02-09 15:33:33.699', '2017-02-09 15:33:33.699', 2, 'Machinery components', 22);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (24, '2017-02-09 15:33:33.701', '2017-02-09 15:33:33.701', 2, 'Machinery equipment/tools', 22);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (25, '2017-02-09 15:33:33.705', '2017-02-09 15:33:33.705', 2, 'Manufacture of machinery', 22);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (26, '2017-02-09 15:33:33.707', '2017-02-09 15:33:33.707', 2, 'Maritime', 22);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (27, '2017-02-09 15:33:33.71', '2017-02-09 15:33:33.71', 3, 'Aluminium and steel workboats', 26);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (28, '2017-02-09 15:33:33.712', '2017-02-09 15:33:33.712', 3, 'Boat/Yacht building', 26);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (29, '2017-02-09 15:33:33.715', '2017-02-09 15:33:33.715', 3, 'Ship repair and conversion', 26);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (30, '2017-02-09 15:33:33.718', '2017-02-09 15:33:33.718', 2, 'Metal structures', 22);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (31, '2017-02-09 15:33:33.721', '2017-02-09 15:33:33.721', 2, 'Other', 22);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (32, '2017-02-09 15:33:33.723', '2017-02-09 15:33:33.723', 2, 'Repair and maintenance service', 22);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (33, '2017-02-09 15:33:33.729', '2017-02-09 15:33:33.729', 1, 'Metalworking', 1);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (34, '2017-02-09 15:33:33.731', '2017-02-09 15:33:33.731', 2, 'Construction of metal structures', 33);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (35, '2017-02-09 15:33:33.736', '2017-02-09 15:33:33.736', 2, 'Houses and buildings', 33);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (36, '2017-02-09 15:33:33.74', '2017-02-09 15:33:33.74', 2, 'Metal products', 33);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (37, '2017-02-09 15:33:33.745', '2017-02-09 15:33:33.745', 2, 'Metal works', 33);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (38, '2017-02-09 15:33:33.751', '2017-02-09 15:33:33.751', 3, 'CNC-machining', 37);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (39, '2017-02-09 15:33:33.755', '2017-02-09 15:33:33.755', 3, 'Forgings, Fasteners', 37);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (40, '2017-02-09 15:33:33.758', '2017-02-09 15:33:33.758', 3, 'Gas, Plasma, Laser cutting', 37);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (41, '2017-02-09 15:33:33.761', '2017-02-09 15:33:33.761', 3, 'MIG, TIG, Aluminum welding', 37);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (42, '2017-02-09 15:33:33.764', '2017-02-09 15:33:33.764', 1, 'Plastic and Rubber', 1);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (43, '2017-02-09 15:33:33.768', '2017-02-09 15:33:33.768', 2, 'Packaging', 42);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (44, '2017-02-09 15:33:33.786', '2017-02-09 15:33:33.786', 2, 'Plastic goods', 42);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (45, '2017-02-09 15:33:33.791', '2017-02-09 15:33:33.791', 2, 'Plastic processing technology', 42);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (46, '2017-02-09 15:33:33.795', '2017-02-09 15:33:33.795', 3, 'Blowing', 45);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (47, '2017-02-09 15:33:33.799', '2017-02-09 15:33:33.799', 3, 'Moulding', 45);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (48, '2017-02-09 15:33:33.801', '2017-02-09 15:33:33.801', 3, 'Plastics welding and processing', 45);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (49, '2017-02-09 15:33:33.804', '2017-02-09 15:33:33.804', 2, 'Plastic profiles', 42);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (50, '2017-02-09 15:33:33.807', '2017-02-09 15:33:33.807', 1, 'Printing', 1);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (51, '2017-02-09 15:33:33.811', '2017-02-09 15:33:33.811', 2, 'Advertising', 50);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (52, '2017-02-09 15:33:33.813', '2017-02-09 15:33:33.813', 2, 'Book/Periodicals printing', 50);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (53, '2017-02-09 15:33:33.816', '2017-02-09 15:33:33.816', 2, 'Labelling and packaging printing', 50);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (54, '2017-02-09 15:33:33.818', '2017-02-09 15:33:33.818', 1, 'Textile and Clothing', 1);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (55, '2017-02-09 15:33:33.821', '2017-02-09 15:33:33.821', 2, 'Clothing', 54);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (56, '2017-02-09 15:33:33.824', '2017-02-09 15:33:33.824', 2, 'Textile', 54);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (57, '2017-02-09 15:33:33.826', '2017-02-09 15:33:33.826', 1, 'Wood', 1);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (58, '2017-02-09 15:33:33.829', '2017-02-09 15:33:33.829', 2, 'Other (Wood)', 57);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (59, '2017-02-09 15:33:33.831', '2017-02-09 15:33:33.831', 2, 'Wooden building materials', 57);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (60, '2017-02-09 15:33:33.85', '2017-02-09 15:33:33.85', 2, 'Wooden houses', 57);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (61, '2017-02-09 15:33:33.853', '2017-02-09 15:33:33.853', 0, 'Other', NULL);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (62, '2017-02-09 15:33:33.856', '2017-02-09 15:33:33.856', 1, 'Creative industries', 61);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (63, '2017-02-09 15:33:33.861', '2017-02-09 15:33:33.861', 1, 'Energy technology', 61);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (64, '2017-02-09 15:33:33.865', '2017-02-09 15:33:33.865', 1, 'Environment', 61);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (65, '2017-02-09 15:33:33.868', '2017-02-09 15:33:33.868', 0, 'Service', NULL);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (66, '2017-02-09 15:33:33.87', '2017-02-09 15:33:33.87', 1, 'Business services', 65);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (67, '2017-02-09 15:33:33.876', '2017-02-09 15:33:33.876', 1, 'Engineering', 65);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (68, '2017-02-09 15:33:33.878', '2017-02-09 15:33:33.878', 1, 'Information Technology and Telecommunications', 65);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (69, '2017-02-09 15:33:33.88', '2017-02-09 15:33:33.88', 2, 'Data processing, Web portals, E-marketing', 68);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (70, '2017-02-09 15:33:33.883', '2017-02-09 15:33:33.883', 2, 'Programming, Consultancy', 68);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (71, '2017-02-09 15:33:33.885', '2017-02-09 15:33:33.885', 2, 'Software, Hardware', 68);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (72, '2017-02-09 15:33:33.887', '2017-02-09 15:33:33.887', 2, 'Telecommunications', 68);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (73, '2017-02-09 15:33:33.89', '2017-02-09 15:33:33.89', 1, 'Tourism', 65);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (74, '2017-02-09 15:33:33.892', '2017-02-09 15:33:33.892', 1, 'Translation services', 65);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (75, '2017-02-09 15:33:33.894', '2017-02-09 15:33:33.894', 1, 'Transport and Logistics', 65);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (76, '2017-02-09 15:33:33.896', '2017-02-09 15:33:33.896', 2, 'Air', 75);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (77, '2017-02-09 15:33:33.899', '2017-02-09 15:33:33.899', 2, 'Rail', 75);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (78, '2017-02-09 15:33:33.901', '2017-02-09 15:33:33.901', 2, 'Road', 75);
INSERT INTO sector (id, created_at, updated_at, level, title, parent_id) VALUES (79, '2017-02-09 15:33:33.905', '2017-02-09 15:33:33.905', 2, 'Water', 75);


--
-- TOC entry 2160 (class 0 OID 0)
-- Dependencies: 190
-- Name: sector_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('sector_id_seq', 79, true);


--
-- TOC entry 2023 (class 2606 OID 58920)
-- Name: employee pk_employee; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY employee
    ADD CONSTRAINT pk_employee PRIMARY KEY (id);


--
-- TOC entry 2025 (class 2606 OID 58922)
-- Name: sector pk_sector; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY sector
    ADD CONSTRAINT pk_sector PRIMARY KEY (id);


--
-- TOC entry 2020 (class 2606 OID 58902)
-- Name: schema_version schema_version_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY schema_version
    ADD CONSTRAINT schema_version_pk PRIMARY KEY (installed_rank);


--
-- TOC entry 2021 (class 1259 OID 58903)
-- Name: schema_version_s_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX schema_version_s_idx ON schema_version USING btree (success);


--
-- TOC entry 2027 (class 2606 OID 58933)
-- Name: employee_sectors fk_employee_sectors_employee; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY employee_sectors
    ADD CONSTRAINT fk_employee_sectors_employee FOREIGN KEY (employee_id) REFERENCES employee(id);


--
-- TOC entry 2026 (class 2606 OID 58923)
-- Name: employee_sectors fk_employee_sectors_sector; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY employee_sectors
    ADD CONSTRAINT fk_employee_sectors_sector FOREIGN KEY (sectors_id) REFERENCES sector(id);


--
-- TOC entry 2028 (class 2606 OID 58928)
-- Name: sector fk_sector; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY sector
    ADD CONSTRAINT fk_sector FOREIGN KEY (parent_id) REFERENCES sector(id);


-- Completed on 2017-02-10 08:32:41

--
-- PostgreSQL database dump complete
--